app.config( [ '$locationProvider', function( $locationProvider ) {
   // In order to get the query string from the
   // $location object, it must be in HTML5 mode.
   $locationProvider.html5Mode({
 		enabled: true,
	 	requireBase: false
	});
}]);

app.controller('FormDataController', ['$scope', '$http', '$location', function ( $scope, $http, $location ) {

	$scope.errors = {};
	$scope.response = {};
	$scope.data = {};

	$scope.page_id = jQuery('#page_id').val();

	$scope.post_action_destination_type = '';
	$scope.post_action_destination = '';

	$scope.getClientData = function ( data ) {

		$http.post( API_URL_GET_CLIENT_PROPERTIES ).then( 
			function ( results ) {
				
				data = results.data;

				if ( typeof data.dob !== 'undefined' ) {
					data.dob = moment(data.dob).format('MM/DD/YYYY');
				}

				date_fields = { 
						asset : [ 'appraisal_date', 'appraisal_expiry_date' ],
						employment : [ 'date_of_employment' ],
						identification : [ 'issue_date', 'expiry_date' ]
					};

				for ( entity in date_fields ) {

					fields = date_fields[entity];

					for ( field_index = 0; field_index < fields.length; field_index++ ) {

						field = fields[field_index];

						for ( entry_index = 0; entry_index < data[entity].length; entry_index++ ) {

							if ( typeof data[entity][entry_index][field] !== 'undefined' ) {

								data[entity][entry_index][field] = moment(data[entity][entry_index][field]).format('MM/DD/YYYY');

							}

						}

					}

				}

				data['principal'] = $location.search().amount;
				data['loanTerm'] = $location.search().term;
				data['loan_term'] = data['loanTerm'];
				data['term'] = data['loanTerm'];
				data['repayment_frequency']	= $location.search().period;
				data['loan_term_period'] = data['repayment_frequency'].replace('ly','s');

				$scope.data = data;
			
			}, 
			function () {} );

	};

	$scope.getClientData();

	$scope.saveFormData = function ( data ) {

		form_action = $scope.form_action;

		if ( !form_action ) form_action = 'save';

		$http.post( API_URL_POST_FORM_DATA, { action : form_action, data : data, current_tab : $scope.activeTab, page_id : $scope.page_id } ).then( 
			function ( results ) {
				
				if ( typeof results.data.errors !== 'undefined' ) {
					
					$scope.errors = results.data.errors;

				} else {

					$scope.errors = {};

					if ( typeof results.data.show_start_application_widget !== 'undefined' ) {
					
						$scope.show_start_application_widget = results.data.show_start_application_widget;
						$scope.loan_request_results = results.data;

					}

					if ( typeof results.data.loan_request !== 'undefined' ) {
					
						$scope.loan_request = results.data.loan_request;
						$scope.data.loan_request = results.data.loan_request;
						$scope.data.loan_id = results.data.loan_id;
						$scope.page_parameters = 'loan_id=' + results.data.loan_id;

					}

					if ( typeof results.data.loan_submitted !== 'undefined' ) {
					
						$scope.loanSubmitted = true;

					}

					if ( typeof results.data.record !== 'undefined' ) {
					
						$scope.data = results.data.record;

					}

					// location.assign( response.data.redirect );

					$scope.doPostAction();

				}
			
			}, 
			function () {} );

	};

	$scope.sendButtonAction = function ( action, data ) {

		$scope.form_action = action;

		if ( action == 'calculate_instalment' ) {

			$scope.show_start_application_widget = false;

		}

		$scope.saveFormData( $scope.data );

	};

	$scope.getPage = function ( tab_id, page_url )
	{

		// Validate data in the first tab first, and then switch to the next tab if successful.

		$http.post( API_URL_POST_FORM_DATA, { action : 'validate', data : $scope.data, target_tab : tab_id, current_tab : $scope.activeTab, page_id : $scope.page_id } ).then( 
			function ( results ) {
				
				if ( typeof results.data.errors !== 'undefined' ) {
					
					$scope.errors = results.data.errors;

				} else {

					location.assign( page_url + '?&' + $scope.page_parameters );

				}

				if ( typeof results.data.response !== 'undefined' ) {
					
					$scope.response = results.data.response;

				}

			}, 
			function () {} );		


	};

	$scope.getTab = function ( tab_id )
	{

		// Validate data in the first tab first, and then switch to the next tab if successful.

		$http.post( API_URL_POST_FORM_DATA, { action : 'validate', data : $scope.data, target_tab : tab_id, current_tab : $scope.activeTab, page_id : $scope.page_id } ).then( 
			function ( results ) {
				
				if ( typeof results.data.errors !== 'undefined' ) {
					
					$scope.errors = results.data.errors;

				} else {

					$scope.activeTab = tab_id;

				}

				if ( typeof results.data.response !== 'undefined' ) {
					
					$scope.response = results.data.response;

				}

			}, 
			function () {} );		


	};

	$scope.getPreviousTab = function ( tab_id )
	{

		$scope.activeTab = tab_id;

	};

	$scope.postAction = function ( destination_type, destination )
	{

		$scope.post_action_destination_type = destination_type;
		$scope.post_action_destination = destination;

	};

	$scope.doPostAction = function ()
	{

		if ( $scope.post_action_destination_type == 'url' ) {

			location.assign( $scope.post_action_destination );
		
		};

		if ( $scope.post_action_destination_type == 'tab' ) {

			$scope.getTab( $scope.post_action_destination );
		
		};

	};

	$scope.addEntityItem = function ( parent, list, type ) {

		if ( typeof $scope[parent][list] === 'undefined' ||  typeof $scope[parent][list] === 'null' ) {
			$scope[parent][list] = [];
		}

		if ( typeof $scope[parent][list][type] === 'undefined' ||  typeof $scope[parent][list][type] === 'null' ) {
			$scope[parent][list][type] = [];
		}

		$scope[parent][list][type].push({ id: 0 });

	};

	$scope.removeEntityItem = function ( parent, list, type, index ) {

		if ( typeof $scope[parent][list] === 'undefined' ||  typeof $scope[parent][list] === 'null' ) {
			$scope[parent][list] = [];
		}

		if ( typeof $scope[parent][list][type] === 'undefined' ||  typeof $scope[parent][list][type] === 'null' ) {
			$scope[parent][list][type] = [];
		}

		$scope[list][type].splice( index, 1 );

	};

	$scope.uploadClientFile = function(files, meta1, meta2, target, category) {
 
			// console.log('get there')
	    var fd = new FormData();
	    fd.append("clientFile", files[0]);
	    fd.append("action", 'upload_client_file');
	    fd.append("page_id", $scope.page_id);
	    fd.append("meta1", angular.toJson( meta1 ) );
	    fd.append("meta2", angular.toJson( meta2 ) );
	    fd.append("category", category );

	    $http.post( API_URL_POST_FORM_DATA, fd, {
	        withCredentials: true,
	        headers: {'Content-Type': undefined },
	        transformRequest: angular.identity
	    }).success( function( response ) {

	    	if ( typeof response.errors !== 'undefined' ) {

	    		_error( response.errors );

	    	} else if ( typeof response.public_path === 'undefined' ) {

	    		_error( _tt( 'Sorry, we could not upload your file due to a system error. Please retry.' ) );

	    	} else {

		    	// _notifySuccess( _tt( 'File upload was successful.' ) );

		    	if ( typeof response.update !== 'undefined' ) {

		    		$target = $scope[target[0]];

		    		for ( t = 1; t < target.length; t++ ) {

		    			if ( typeof $target[target[t]] !== 'undefined' ) {

		    				$target = $target[target[t]];

		    			}

		    		}

		    		target_field = response.update.field;
		    		target_value = response.update.value;

		    		$target[target_field] = target_value;

		    	}

		    }

	    } ).error( function() {} );

		};

	$scope.lists = [];

	$scope.getLists = function() {

		$http.get( API_URL_GET_LISTS ).then( 
			function ( results ) {
				
				$scope.lists = results.data;

				$scope.lists.empty_list = [ 
												  {label: 'NA', value: ''}, 
											];


				$scope.lists.countries = [ 
															  {label: 'Afghanistan', value: 'AF'}, 
															  {label: 'Åland Islands', value: 'AX'}, 
															  {label: 'Albania', value: 'AL'}, 
															  {label: 'Algeria', value: 'DZ'}, 
															  {label: 'American Samoa', value: 'AS'}, 
															  {label: 'AndorrA', value: 'AD'}, 
															  {label: 'Angola', value: 'AO'}, 
															  {label: 'Anguilla', value: 'AI'}, 
															  {label: 'Antarctica', value: 'AQ'}, 
															  {label: 'Antigua and Barbuda', value: 'AG'}, 
															  {label: 'Argentina', value: 'AR'}, 
															  {label: 'Armenia', value: 'AM'}, 
															  {label: 'Aruba', value: 'AW'}, 
															  {label: 'Australia', value: 'AU'}, 
															  {label: 'Austria', value: 'AT'}, 
															  {label: 'Azerbaijan', value: 'AZ'}, 
															  {label: 'Bahamas', value: 'BS'}, 
															  {label: 'Bahrain', value: 'BH'}, 
															  {label: 'Bangladesh', value: 'BD'}, 
															  {label: 'Barbados', value: 'BB'}, 
															  {label: 'Belarus', value: 'BY'}, 
															  {label: 'Belgium', value: 'BE'}, 
															  {label: 'Belize', value: 'BZ'}, 
															  {label: 'Benin', value: 'BJ'}, 
															  {label: 'Bermuda', value: 'BM'}, 
															  {label: 'Bhutan', value: 'BT'}, 
															  {label: 'Bolivia', value: 'BO'}, 
															  {label: 'Bosnia and Herzegovina', value: 'BA'}, 
															  {label: 'Botswana', value: 'BW'}, 
															  {label: 'Bouvet Island', value: 'BV'}, 
															  {label: 'Brazil', value: 'BR'}, 
															  {label: 'British Indian Ocean Territory', value: 'IO'}, 
															  {label: 'Brunei Darussalam', value: 'BN'}, 
															  {label: 'Bulgaria', value: 'BG'}, 
															  {label: 'Burkina Faso', value: 'BF'}, 
															  {label: 'Burundi', value: 'BI'}, 
															  {label: 'Cambodia', value: 'KH'}, 
															  {label: 'Cameroon', value: 'CM'}, 
															  {label: 'Canada', value: 'CA'}, 
															  {label: 'Cape Verde', value: 'CV'}, 
															  {label: 'Cayman Islands', value: 'KY'}, 
															  {label: 'Central African Republic', value: 'CF'}, 
															  {label: 'Chad', value: 'TD'}, 
															  {label: 'Chile', value: 'CL'}, 
															  {label: 'China', value: 'CN'}, 
															  {label: 'Christmas Island', value: 'CX'}, 
															  {label: 'Cocos (Keeling) Islands', value: 'CC'}, 
															  {label: 'Colombia', value: 'CO'}, 
															  {label: 'Comoros', value: 'KM'}, 
															  {label: 'Congo', value: 'CG'}, 
															  {label: 'Congo, The Democratic Republic of the', value: 'CD'}, 
															  {label: 'Cook Islands', value: 'CK'}, 
															  {label: 'Costa Rica', value: 'CR'}, 
															  {label: 'Cote D\'Ivoire', value: 'CI'}, 
															  {label: 'Croatia', value: 'HR'}, 
															  {label: 'Cuba', value: 'CU'}, 
															  {label: 'Cyprus', value: 'CY'}, 
															  {label: 'Czech Republic', value: 'CZ'}, 
															  {label: 'Denmark', value: 'DK'}, 
															  {label: 'Djibouti', value: 'DJ'}, 
															  {label: 'Dominica', value: 'DM'}, 
															  {label: 'Dominican Republic', value: 'DO'}, 
															  {label: 'Ecuador', value: 'EC'}, 
															  {label: 'Egypt', value: 'EG'}, 
															  {label: 'El Salvador', value: 'SV'}, 
															  {label: 'Equatorial Guinea', value: 'GQ'}, 
															  {label: 'Eritrea', value: 'ER'}, 
															  {label: 'Estonia', value: 'EE'}, 
															  {label: 'Ethiopia', value: 'ET'}, 
															  {label: 'Falkland Islands (Malvinas)', value: 'FK'}, 
															  {label: 'Faroe Islands', value: 'FO'}, 
															  {label: 'Fiji', value: 'FJ'}, 
															  {label: 'Finland', value: 'FI'}, 
															  {label: 'France', value: 'FR'}, 
															  {label: 'French Guiana', value: 'GF'}, 
															  {label: 'French Polynesia', value: 'PF'}, 
															  {label: 'French Southern Territories', value: 'TF'}, 
															  {label: 'Gabon', value: 'GA'}, 
															  {label: 'Gambia', value: 'GM'}, 
															  {label: 'Georgia', value: 'GE'}, 
															  {label: 'Germany', value: 'DE'}, 
															  {label: 'Ghana', value: 'GH'}, 
															  {label: 'Gibraltar', value: 'GI'}, 
															  {label: 'Greece', value: 'GR'}, 
															  {label: 'Greenland', value: 'GL'}, 
															  {label: 'Grenada', value: 'GD'}, 
															  {label: 'Guadeloupe', value: 'GP'}, 
															  {label: 'Guam', value: 'GU'}, 
															  {label: 'Guatemala', value: 'GT'}, 
															  {label: 'Guernsey', value: 'GG'}, 
															  {label: 'Guinea', value: 'GN'}, 
															  {label: 'Guinea-Bissau', value: 'GW'}, 
															  {label: 'Guyana', value: 'GY'}, 
															  {label: 'Haiti', value: 'HT'}, 
															  {label: 'Heard Island and Mcdonald Islands', value: 'HM'}, 
															  {label: 'Holy See (Vatican City State)', value: 'VA'}, 
															  {label: 'Honduras', value: 'HN'}, 
															  {label: 'Hong Kong', value: 'HK'}, 
															  {label: 'Hungary', value: 'HU'}, 
															  {label: 'Iceland', value: 'IS'}, 
															  {label: 'India', value: 'IN'}, 
															  {label: 'Indonesia', value: 'ID'}, 
															  {label: 'Iran, Islamic Republic Of', value: 'IR'}, 
															  {label: 'Iraq', value: 'IQ'}, 
															  {label: 'Ireland', value: 'IE'}, 
															  {label: 'Isle of Man', value: 'IM'}, 
															  {label: 'Israel', value: 'IL'}, 
															  {label: 'Italy', value: 'IT'}, 
															  {label: 'Jamaica', value: 'JM'}, 
															  {label: 'Japan', value: 'JP'}, 
															  {label: 'Jersey', value: 'JE'}, 
															  {label: 'Jordan', value: 'JO'}, 
															  {label: 'Kazakhstan', value: 'KZ'}, 
															  {label: 'Kenya', value: 'KE'}, 
															  {label: 'Kiribati', value: 'KI'}, 
															  {label: 'Korea, Democratic People\'S Republic of', value: 'KP'}, 
															  {label: 'Korea, Republic of', value: 'KR'}, 
															  {label: 'Kuwait', value: 'KW'}, 
															  {label: 'Kyrgyzstan', value: 'KG'}, 
															  {label: 'Lao People\'S Democratic Republic', value: 'LA'}, 
															  {label: 'Latvia', value: 'LV'}, 
															  {label: 'Lebanon', value: 'LB'}, 
															  {label: 'Lesotho', value: 'LS'}, 
															  {label: 'Liberia', value: 'LR'}, 
															  {label: 'Libyan Arab Jamahiriya', value: 'LY'}, 
															  {label: 'Liechtenstein', value: 'LI'}, 
															  {label: 'Lithuania', value: 'LT'}, 
															  {label: 'Luxembourg', value: 'LU'}, 
															  {label: 'Macao', value: 'MO'}, 
															  {label: 'Macedonia, The Former Yugoslav Republic of', value: 'MK'}, 
															  {label: 'Madagascar', value: 'MG'}, 
															  {label: 'Malawi', value: 'MW'}, 
															  {label: 'Malaysia', value: 'MY'}, 
															  {label: 'Maldives', value: 'MV'}, 
															  {label: 'Mali', value: 'ML'}, 
															  {label: 'Malta', value: 'MT'}, 
															  {label: 'Marshall Islands', value: 'MH'}, 
															  {label: 'Martinique', value: 'MQ'}, 
															  {label: 'Mauritania', value: 'MR'}, 
															  {label: 'Mauritius', value: 'MU'}, 
															  {label: 'Mayotte', value: 'YT'}, 
															  {label: 'Mexico', value: 'MX'}, 
															  {label: 'Micronesia, Federated States of', value: 'FM'}, 
															  {label: 'Moldova, Republic of', value: 'MD'}, 
															  {label: 'Monaco', value: 'MC'}, 
															  {label: 'Mongolia', value: 'MN'}, 
															  {label: 'Montserrat', value: 'MS'}, 
															  {label: 'Morocco', value: 'MA'}, 
															  {label: 'Mozambique', value: 'MZ'}, 
															  {label: 'Myanmar', value: 'MM'}, 
															  {label: 'Namibia', value: 'NA'}, 
															  {label: 'Nauru', value: 'NR'}, 
															  {label: 'Nepal', value: 'NP'}, 
															  {label: 'Netherlands', value: 'NL'}, 
															  {label: 'Netherlands Antilles', value: 'AN'}, 
															  {label: 'New Caledonia', value: 'NC'}, 
															  {label: 'New Zealand', value: 'NZ'}, 
															  {label: 'Nicaragua', value: 'NI'}, 
															  {label: 'Niger', value: 'NE'}, 
															  {label: 'Nigeria', value: 'NG'}, 
															  {label: 'Niue', value: 'NU'}, 
															  {label: 'Norfolk Island', value: 'NF'}, 
															  {label: 'Northern Mariana Islands', value: 'MP'}, 
															  {label: 'Norway', value: 'NO'}, 
															  {label: 'Oman', value: 'OM'}, 
															  {label: 'Pakistan', value: 'PK'}, 
															  {label: 'Palau', value: 'PW'}, 
															  {label: 'Palestinian Territory, Occupied', value: 'PS'}, 
															  {label: 'Panama', value: 'PA'}, 
															  {label: 'Papua New Guinea', value: 'PG'}, 
															  {label: 'Paraguay', value: 'PY'}, 
															  {label: 'Peru', value: 'PE'}, 
															  {label: 'Philippines', value: 'PH'}, 
															  {label: 'Pitcairn', value: 'PN'}, 
															  {label: 'Poland', value: 'PL'}, 
															  {label: 'Portugal', value: 'PT'}, 
															  {label: 'Puerto Rico', value: 'PR'}, 
															  {label: 'Qatar', value: 'QA'}, 
															  {label: 'Reunion', value: 'RE'}, 
															  {label: 'Romania', value: 'RO'}, 
															  {label: 'Russian Federation', value: 'RU'}, 
															  {label: 'RWANDA', value: 'RW'}, 
															  {label: 'Saint Helena', value: 'SH'}, 
															  {label: 'Saint Kitts and Nevis', value: 'KN'}, 
															  {label: 'Saint Lucia', value: 'LC'}, 
															  {label: 'Saint Pierre and Miquelon', value: 'PM'}, 
															  {label: 'Saint Vincent and the Grenadines', value: 'VC'}, 
															  {label: 'Samoa', value: 'WS'}, 
															  {label: 'San Marino', value: 'SM'}, 
															  {label: 'Sao Tome and Principe', value: 'ST'}, 
															  {label: 'Saudi Arabia', value: 'SA'}, 
															  {label: 'Senegal', value: 'SN'}, 
															  {label: 'Serbia and Montenegro', value: 'CS'}, 
															  {label: 'Seychelles', value: 'SC'}, 
															  {label: 'Sierra Leone', value: 'SL'}, 
															  {label: 'Singapore', value: 'SG'}, 
															  {label: 'Slovakia', value: 'SK'}, 
															  {label: 'Slovenia', value: 'SI'}, 
															  {label: 'Solomon Islands', value: 'SB'}, 
															  {label: 'Somalia', value: 'SO'}, 
															  {label: 'South Africa', value: 'ZA'}, 
															  {label: 'South Georgia and the South Sandwich Islands', value: 'GS'}, 
															  {label: 'Spain', value: 'ES'}, 
															  {label: 'Sri Lanka', value: 'LK'}, 
															  {label: 'Sudan', value: 'SD'}, 
															  {label: 'Suriname', value: 'SR'}, 
															  {label: 'Svalbard and Jan Mayen', value: 'SJ'}, 
															  {label: 'Swaziland', value: 'SZ'}, 
															  {label: 'Sweden', value: 'SE'}, 
															  {label: 'Switzerland', value: 'CH'}, 
															  {label: 'Syrian Arab Republic', value: 'SY'}, 
															  {label: 'Taiwan, Province of China', value: 'TW'}, 
															  {label: 'Tajikistan', value: 'TJ'}, 
															  {label: 'Tanzania, United Republic of', value: 'TZ'}, 
															  {label: 'Thailand', value: 'TH'}, 
															  {label: 'Timor-Leste', value: 'TL'}, 
															  {label: 'Togo', value: 'TG'}, 
															  {label: 'Tokelau', value: 'TK'}, 
															  {label: 'Tonga', value: 'TO'}, 
															  {label: 'Trinidad and Tobago', value: 'TT'}, 
															  {label: 'Tunisia', value: 'TN'}, 
															  {label: 'Turkey', value: 'TR'}, 
															  {label: 'Turkmenistan', value: 'TM'}, 
															  {label: 'Turks and Caicos Islands', value: 'TC'}, 
															  {label: 'Tuvalu', value: 'TV'}, 
															  {label: 'Uganda', value: 'UG'}, 
															  {label: 'Ukraine', value: 'UA'}, 
															  {label: 'United Arab Emirates', value: 'AE'}, 
															  {label: 'United Kingdom', value: 'GB'}, 
															  {label: 'United States', value: 'US'}, 
															  {label: 'United States Minor Outlying Islands', value: 'UM'}, 
															  {label: 'Uruguay', value: 'UY'}, 
															  {label: 'Uzbekistan', value: 'UZ'}, 
															  {label: 'Vanuatu', value: 'VU'}, 
															  {label: 'Venezuela', value: 'VE'}, 
															  {label: 'Viet Nam', value: 'VN'}, 
															  {label: 'Virgin Islands, British', value: 'VG'}, 
															  {label: 'Virgin Islands, U.S.', value: 'VI'}, 
															  {label: 'Wallis and Futuna', value: 'WF'}, 
															  {label: 'Western Sahara', value: 'EH'}, 
															  {label: 'Yemen', value: 'YE'}, 
															  {label: 'Zambia', value: 'ZM'}, 
															  {label: 'Zimbabwe', value: 'ZW'} 
															];

				$scope.lists.headingsizes = [ 
				  {label: 'H1', value: 'h1'}, 
				  {label: 'H2', value: 'h2'}, 
				  {label: 'H3', value: 'h3'}, 
				  {label: 'H4', value: 'h4'}, 
				  {label: 'H5', value: 'h5'}, 
				  {label: 'H6', value: 'h6'}, 
				];

			}, 
			function () {} );	

	};

	$scope.getLists();

}]);