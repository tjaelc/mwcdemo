/* HTML document is loaded. DOM is ready.
-------------------------------------------*/
jQuery(function(){

    /* start typed element */
    //http://stackoverflow.com/questions/24874797/select-div-title-text-and-make-array-with-jquery
    var subElementArray = jQuery.map(jQuery('.sub-element'), function(el) { return jQuery(el).text(); });    
    jQuery(".element").typed({
        strings: subElementArray,
        typeSpeed: 30,
        contentType: 'html',
        showCursor: false,
        loop: true,
        loopCount: true,
    });
    /* end typed element */

    /* Smooth scroll and Scroll spy (https://github.com/ChrisWojcik/single-page-nav)    
    ---------------------------------------------------------------------------------*/ 
    jQuery('.templatemo-nav').singlePageNav({
        offset: jQuery(".templatemo-nav").height(),
        filter: ':not(.external)',
        updateHash: false
    });

    /* start navigation top js */
    jQuery(window).scroll(function(){
        if(jQuery(this).scrollTop()>58){
            jQuery(".templatemo-nav").addClass("sticky");
        }
        else{
            jQuery(".templatemo-nav").removeClass("sticky");
        }
    });
    
    /* Hide mobile menu after clicking on a link
    -----------------------------------------------*/
    jQuery('.navbar-collapse a').click(function(){
        jQuery(".navbar-collapse").collapse('hide');
    });
    /* end navigation top js */

    jQuery('body').bind('touchstart', function() {});

    /* wow
    -----------------*/
    new WOW().init();
});

/* start preloader */
jQuery(window).load(function(){
	jQuery('.preloader').fadeOut(1000); // set duration in brackets    
});
/* end preloader */
