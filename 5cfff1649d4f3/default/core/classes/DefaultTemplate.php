<?php

namespace Core\Classes;

use \App\Modules\Core\Classes\Form;

if ( !class_exists( 'Core\Classes\DefaultTemplate' ) ) {

class DefaultTemplate {

	public static function get_template_name() {

		return "this is some string retrieved via class";

	}

	public static function Form () {

		return new Form();

	}

}

}