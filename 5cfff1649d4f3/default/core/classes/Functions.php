<?php

use Core\Classes\DefaultTemplate as Template;

if ( !function_exists( 'footer_links' ) ) {

	function footer_links( $links ) {

		$links = array_values( array_filter( preg_split( "/\n/", $links ) ) );
		$links_parsed = [];

		for ( $i = 0; $i < count( $links ); $i = $i + 2 ) {

			if ( isset( $links[$i+1] ) && isset( $links[$i] ) ) {

				$links_parsed[] = [ $links[$i], $links[$i+1] ];

			}

		}

		$links_parsed = array_map( function ( $l ) { 
			
				$url = $l[1];
				$blank = substr( $url, 0, 4 ) == 'http' ? ' target="_blank"' : '';

				return '<li><a href="' . $url . '"' . $blank . '>' . trim( $l[0] ) . '</a></li>'; 

			}, $links_parsed );

		return implode( "\n", $links_parsed );

	}

}

if ( !function_exists( 'paragraphs' ) ) {

	function paragraphs( $content ) {

		$content = nl2br( $content );

		$content = '<p>' . preg_replace('#(<br />[\r\n]+){2}#', '</p><p>', $content ) . '</p>';

		return $content;

	}

}

if ( !function_exists( 'menu' ) ) {

	function menu( $args ) {

		if ( !is_array( $args ) ) $args = [ 'slug' => $args ];

		$slug = $args['slug'];
		$links = Menu::get_menu( $slug );
		$menu = array_map( function( $link ) { $class = !stristr( $link['url'], '#' ) ? '' : 'samepage'; return '<li><a href="' . $link['url'] . '" class="' . $class . '">' . $link['title'] . '</a></li>'; }, $links );
		$menu = implode( "\n", $menu );

		return $menu;

	}

}

if ( !function_exists( 'print_form' ) ) {

	function print_form( $args ) {

		if ( !is_array( $args ) ) $args = [ 'slug' => $args ];

		$slug = $args['slug'];

		return Template::Form()::printForm( $slug );

	}

}


if ( !function_exists( 'display_admin_link' ) ) {

	function display_admin_link() { 

		if ( is_preview() ?? false ) {

			return '<div style="position:fixed; width: 100%; padding: 7px; text-align: center; bottom: 0px; background: rgba(80, 4, 34, 0.75); border-top: dottted 2px #888; color: #FFF !important">
		  		<strong>Some functionalities such as loan processing are disabled in PREVIEW mode until you publish your website.</strong> <a href="/admin/publish" class="btn btn-warning">' . ___("Go to Publishing") . '</a> <a href="/livesite" class="btn btn-success">' . ___("View Live Site") . '</a>
		  	</div>';

		
		} elseif ( is_admin() ?? false ) {

			return '<div style="position:fixed; width: 100%; padding: 7px; text-align: center; bottom: 0px; background: rgba(36, 63, 80, 0.5); border-top: dottted 2px #888; color: #FFF !important">
		  		<strong>You are logged in as an admin.</strong> <a href="/admin" class="btn btn-warning">' . ___("Go to Admin Section") . '</a>
		  	</div>';

		
		}

	}

}


if ( !function_exists( 'get_institutes' ) ) {

	function get_institutes( $id ) {

		if ( !is_array( $args ) ) $args = [ 'slug' => $args ];

		$slug = $args['slug'];
		$links = Menu::get_menu( $slug );
		$menu = array_map( function( $link ) { $class = !stristr( $link['url'], '#' ) ? '' : 'samepage'; return '<li><a href="' . $link['url'] . '" class="' . $class . '">' . $link['title'] . '</a></li>'; }, $links );
		$menu = implode( "\n", $menu );

		return $menu;

	}

}