<?php if ( !defined( 'TEMPLATE') ) exit(); ?>
<!DOCTYPE HTML>
<html>
	<head>
		<title><? get_section( 'title' ) ?> | <? get_section( 'company_name' ) ?></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="<? base_path( '/core/static/js/ie/html5shiv.js' ) ?>"></script><![endif]-->
		<link rel="stylesheet" href="<? base_path( '/core/static/css/bootstrap.css' ) ?>" />
		<link rel="stylesheet" href="<? base_path( '/core/static/css/main.css' ) ?>" />
		<!--[if lte IE 8]><link rel="stylesheet" href="<? base_path( '/core/static/css/ie8.css' ) ?>" /><![endif]-->
		<link rel="stylesheet" href="/js/vendor/node_modules/angular-material/angular-material.css">

    <@CSS_EXPORT>

	</head>
	<body class="left-sidebar" ng-app="reachdefault">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<header id="header" class="container">

						<!-- Logo -->
							<div id="logo">
								<h1><a href="/"><? get_section( 'company_name' ) ?></a></h1>
							</div>

						<!-- Nav -->
							<nav id="nav">
								<ul>
									<? menu( 'main-menu' ) ?>
								</ul>
							</nav>

					</header>
				</div>

			<!-- Main -->
				<div id="main-wrapper">
					<div class="container">
						<div class="row 200%">
							<div class="2u 12u$(medium) important(medium)">
							</div>
							<div class="8u 12u$(medium) important(medium)">
								<div id="content">

									@layout()

								</div>
							</div>
							<div class="2u 12u$(medium) important(medium)">
							</div>
						</div>
					</div>
				</div>

			<!-- Footer -->
			@include( 'footer' )

			</div>

		<!-- Scripts -->

			@include('scripts')

	</body>
</html>