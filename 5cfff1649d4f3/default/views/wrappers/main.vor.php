<?php if ( !defined( 'TEMPLATE') ) exit(); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!-- 
        Awesome Template
        http://www.templatemo.com/preview/templatemo_450_awesome
        -->
        <title><? get_section( 'title' ) ?> | <? get_section( 'company_name' ) ?></title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link rel="stylesheet" href="<? base_path( '/core/static/css/animate.min.css' ) ?>">
		<link rel="stylesheet" href="<? base_path( '/core/static/css/bootstrap.min.css' ) ?>">
		<link rel="stylesheet" href="<? base_path( '/core/static/css/font-awesome.min.css' ) ?>">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<? base_path( '/assets/css/default.css' ) ?>">

    <@CSS_EXPORT>
		
<style>

.content-body select {
  height: 44px;
  font-size: 16px;
  width: 100%;
  margin-bottom: 10px;
  background: #fff;
  border: 1px solid #d9d9d9;
  border-top: 1px solid #c0c0c0;
  /* border-radius: 2px; */
  padding: 0 8px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;

}

.content-body input[type=text], input[type=password] {
  height: 44px;
  font-size: 16px;
  width: 100%;
  margin-bottom: 10px;
  -webkit-appearance: none;
  background: #fff;
  border: 1px solid #d9d9d9;
  border-top: 1px solid #c0c0c0;
  /* border-radius: 2px; */
  padding: 0 8px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
}

.content-body input[type=text]:hover, input[type=password]:hover {
  border: 1px solid #b9b9b9;
  border-top: 1px solid #a0a0a0;
  -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
}

@include('colours')
</style>
	</head>
	<body id="top" ng-app="reachdefault">

        <!-- start preloader -->
        <div class="preloader">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
        </div>
        <!-- end preloader -->

        <?php

        if ( false ):

        ?>

        <!-- start header -->
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <p>
                            <i class="fa fa-phone"></i><span> {{___('Phone')}}</span><? get_section( 'phone_number', true ) ?>
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <p><i class="fa fa-envelope-o"></i><span> {{___('Email')}}</span><a href="#"><? get_section( 'email_address', true ) ?></a></p>
                    </div>
                    <div class="col-md-5 col-sm-4 col-xs-12">
                        <ul class="social-icon">
                            <li><span>{{___('Meet us on')}}</span></li>
                            <? get_section( 'twitter_handle' ) ? '<li><a href="https://twitter.com/' . get_section( 'twitter_handle' ) . '" class="fa fa-twitter"><span class="label"></span></a></li>' : '' ?>
                            <? get_section( 'facebook_handle' ) ? '<li><a href="https://facebook.com/' . get_section( 'facebook_handle' ) . '" class="fa fa-facebook"><span class="label"></span></a></li>' : '' ?>
                            <? get_section( 'instagram_handle' ) ? '<li><a href="https://instagram.com/' . get_section( 'instagram_handle' ) . '" class="fa fa-instagram"><span class="label"></span></a></li>' : '' ?>
                            <? get_section( 'dribbble_handle' ) ? '<li><a href="https://dribbble.com/' . get_section( 'dribbble_handle' ) . '" class="fa fa-dribbble"><span class="label"></span></a></li>' : '' ?>
                            <? get_section( 'pinterest_handle' ) ? '<li><a href="https://pinterest.com/' . get_section( 'pinterest_handle' ) . '" class="fa fa-pinterest"><span class="label"></span></a></li>' : '' ?>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <!-- end header -->

      <?php endif; ?>

        <!-- start navigation -->
        <nav class="navbar navbar-default templatemo-nav" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon icon-bar"></span>
                        <span class="icon icon-bar"></span>
                        <span class="icon icon-bar"></span>
                    </button>
                    <a href="/" class="navbar-brand"><? \App\Setting::get( 'flag_display_company_logo' ) ? ( \App\Setting::get( 'logo_path' ) ? '<img src="' . \App\Setting::get( 'logo_path' ) . '" style="height: 50px" class="pull-left" /> &nbsp; ' : '' ) : '' ?> <? \App\Setting::get( 'flag_display_company_name' ) ? get_section( 'company_name', true ) : '' ?></a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <? menu( 'general-main-menu' ) ?>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- end navigation -->

    	<div class="content-body">

            <div class="container wow bounceIn">

                <div class="row">

                    <div class="col-md-10 col-md-offset-1">

                        @layout()

                    </div>

                </div>

            </div>

        </div>

        <!-- start copyright -->
        <footer id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p class="wow bounceIn" data-wow-offset="50" data-wow-delay="0.3s">
                        <? get_section('copyright', true) ?> - <? get_section('company_name', true) ?></p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end copyright -->

        <? display_admin_link() ?>

        @include('scripts')

	</body>
</html>