<?php if ( !defined( 'TEMPLATE') ) exit(); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<!--
        Awesome Template
        http://www.templatemo.com/preview/templatemo_450_awesome
        -->
		<title><? get_section( 'title' ) ?> | <? get_section( 'company_name' ) ?></title>
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" href="<? base_path( '/core/static/css/animate.min.css' ) ?>">
        <link rel="stylesheet" href="<? base_path( '/core/static/css/bootstrap.min.css' ) ?>">
        <link rel="stylesheet" href="<? base_path( '/core/static/css/font-awesome.min.css' ) ?>">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<? base_path( '/core/static/css/templatemo-style.css' ) ?>">
    <link rel="stylesheet" href="<? base_path( '/assets/css/default.css' ) ?>">

 <style>

        #home {
            background: url(<? get_section('home_page_lead_image') ?>) no-repeat;
            background-size: cover;
        }
        .mb-2 {margin-bottom: 8px !important;}


.form label {

    color: #000;

}

.form input[type=text], input[type=password] {
  height: 44px;
  font-size: 16px;
  width: 100%;
  margin-bottom: 10px;
  -webkit-appearance: none;
  background: #fff;
  border: 1px solid #d9d9d9;
  border-top: 1px solid #c0c0c0;
  /* border-radius: 2px; */
  padding: 0 8px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
}

.form input[type=text]:hover, input[type=password]:hover {
  border: 1px solid #b9b9b9;
  border-top: 1px solid #a0a0a0;
  -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
}

@include('colours')

a.applyNowBtn.disabled {
          pointer-events: none;
          cursor: default;
          opacity: 0.4;
        }

</style>
<@CSS_EXPORT>


<script src="<? base_path( '/core/static/js/jquery.js' ) ?>"></script>
<script src="<? base_path( '/core/static/js/bootstrap.min.js' ) ?>"></script>
<script src="<? base_path( '/core/static/js/jquery.singlePageNav.min.js' ) ?>"></script>
<script src="<? base_path( '/core/static/js/typed.js' ) ?>"></script>
<script src="<? base_path( '/core/static/js/wow.min.js' ) ?>"></script>
<script src="<? base_path( '/core/static/js/custom.js' ) ?>"></script>
</head>
	<body id="top" class="homepage-body" ng-app="ReachHome" ng-controller="HomePageController" >

		<!-- start preloader -->
		<div class="preloader">
			<div class="sk-spinner sk-spinner-wave">
     	 		<div class="sk-rect1"></div>
       			<div class="sk-rect2"></div>
       			<div class="sk-rect3"></div>
      	 		<div class="sk-rect4"></div>
      			<div class="sk-rect5"></div>
     		</div>
    	</div>
    	<!-- end preloader -->

        <!-- start header -->
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <p>
                            <i class="fa fa-phone"></i><span> {{___('Phone')}}</span><? get_section( 'phone_number', true ) ?>
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <p><i class="fa fa-envelope-o"></i><span> {{___('Email')}}</span><a href="#"><? get_section( 'email_address', true ) ?></a></p>
                    </div>
                    <div class="col-md-5 col-sm-4 col-xs-12">
                        <ul class="social-icon">
                            <li><span>{{___('Meet us on')}}</span></li>
                            <? get_section( 'twitter_handle' ) ? '<li><a href="https://twitter.com/' . get_section( 'twitter_handle' ) . '" class="fa fa-twitter"><span class="label"></span></a></li>' : '' ?>
                            <? get_section( 'facebook_handle' ) ? '<li><a href="https://facebook.com/' . get_section( 'facebook_handle' ) . '" class="fa fa-facebook"><span class="label"></span></a></li>' : '' ?>
                            <? get_section( 'instagram_handle' ) ? '<li><a href="https://instagram.com/' . get_section( 'instagram_handle' ) . '" class="fa fa-instagram"><span class="label"></span></a></li>' : '' ?>
                            <? get_section( 'dribbble_handle' ) ? '<li><a href="https://dribbble.com/' . get_section( 'dribbble_handle' ) . '" class="fa fa-dribbble"><span class="label"></span></a></li>' : '' ?>
                            <? get_section( 'pinterest_handle' ) ? '<li><a href="https://pinterest.com/' . get_section( 'pinterest_handle' ) . '" class="fa fa-pinterest"><span class="label"></span></a></li>' : '' ?>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <!-- end header -->

    	<!-- start navigation -->
		<nav class="navbar navbar-default templatemo-nav" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
					</button>
					<a href="/" class="navbar-brand"><? \App\Setting::get( 'flag_display_company_logo' ) ? ( \App\Setting::get( 'logo_path' ) ? '<img src="' . \App\Setting::get( 'logo_path' ) . '" style="height: 50px" class="pull-left" /> &nbsp; ' : '' ) : '' ?> <? \App\Setting::get( 'flag_display_company_name' ) ? get_section( 'company_name', true ) : '' ?></a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
						<? menu( 'homepage-main-menu' ) ?>
					</ul>
				</div>
			</div>
		</nav>
		<!-- end navigation -->

    	<!-- start home -->
    	<section id="home">
    		<div class="container">
    			<div class="row" s/tyle="background-color:rgba(3, 49, 78, 0.5); -moz-border-radius: 25px; border-radius: 25px; -webkit-border-radius: 25px; -khtml-border-radius: 25px; padding-bottom: 25px">
    				<div class="col-md-offset-3 col-md-9 text-center">
    					<h1 class="wow fadeIn" data-wow-offset="50" data-wow-delay="0.9s"><? get_section( 'home_page_heading', true ) ?></h1>
    					<div class="element">
                            <? return implode( "\n", array_map( function( $line ) { return '<div class="sub-element"><strong>' . $line . '</strong></div>'; }, explode( "\n", get_section( 'home_page_blurb', true ) ) ) ); ?>
                        </div>

                        <br/><br/>

                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <center>
                                <label class="lead"><b>Select Institute</b></label></center>
                                <select class="form-control input-lg" ng-model="institute" ng-change="getCourses()">
                                    <option ng-repeat="institute in institutes" value="<%institute.id%>"><%institute.name%></option>
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <center><label class="lead"><b>Select Course</b></label></center>
                                <select class="form-control input-lg" ng-model="course" ng-change="getCourseDetails()">
                                    <option ng-repeat="course in courses" value="<%course.id%>"><%course.name%></option>
                                </select>
                            </div>
                        </div>

                        <br/><br/>

                        <p class="lead" ng-if="courseDetails.name && courseDetails.term  && courseDetails.cost"><b>You have chosen <%courseDetails.name%> for a term of <%courseDetails.term%> months at a cost of <%courseDetails.cost | moneyFormat%> <%courseDetails.currency || 'JMD'%></b></p>
                        <center>
    					<a data-scroll href="<? get_section( 'home_page_lead_button_link' ) ?>?amount=<%courseDetails.cost%>&term=<%courseDetails.term%>&period=months" class="btn btn-default applyNowBtn wow fadeInUp" data-wow-offset="50" data-wow-delay="0.6s"><? get_section( 'home_page_lead_button_text', true ) ?></a></center>
    				</div>
    			</div>
    		</div>
    	</section>
    	<!-- end home -->

    	<!-- start about -->
		<section id="about">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
    					<h2 class="wow bounceIn" data-wow-offset="50" data-wow-delay="0.3s"><? get_section( 'home_page_about_title', true ) ?></h2>
    				</div>
					<div class="col-md-4 col-sm-4 col-xs-12 wow fadeInLeft" data-wow-offset="50" data-wow-delay="0.6s">
						<div class="media">
							<div class="media-heading-wrapper">
								<div class="media-object pull-left">
									<i class="fa fa-star"></i>
								</div>
								<h3 class="media-heading"><? get_section( 'home_page_about_heading_1', true ) ?></h3>
							</div>
							<div class="media-body">
								<p><? get_section( 'home_page_about_text_1', true ) ?></p>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 wow fadeInUp" data-wow-offset="50" data-wow-delay="0.9s">
						<div class="media">
							<div class="media-heading-wrapper">
								<div class="media-object pull-left">
									<i class="fa fa-star"></i>
								</div>
								<h3 class="media-heading"><? get_section( 'home_page_about_heading_2', true ) ?></h3>
							</div>
							<div class="media-body">
								<p><? get_section( 'home_page_about_text_2', true ) ?></p>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 wow fadeInRight" data-wow-offset="50" data-wow-delay="0.6s">
						<div class="media">
							<div class="media-heading-wrapper">
								<div class="media-object pull-left">
									<i class="fa fa-star"></i>
								</div>
								<h3 class="media-heading"><? get_section( 'home_page_about_heading_3', true ) ?></h3>
							</div>
							<div class="media-body">
								<p><? get_section( 'home_page_about_text_3', true ) ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end about -->

    	<!-- start team -->
    	<section id="products">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-12">
    					<h2 class="wow bounceIn" data-wow-offset="50" data-wow-delay="0.3s"><? get_section( 'home_page_products_title', true ) ?></h2>
    				</div>
    				<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="1.3s">
    					<div class="team-wrapper">
    						<!--img src="images/team-img1.jpg" class="img-responsive" alt="team img 1"-->
    							<div class="team-des">
    								<h3><? get_section( 'home_page_products_heading_1', true ) ?></h3>
    								<span class="heading"><? get_section( 'home_page_products_sub_heading_1', true ) ?></span>
    								<p><? get_section( 'home_page_products_description_1', true ) ?></p>
    							</div>
    					</div>
    				</div>
    				<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="1.6s">
    					<div class="team-wrapper">
    						<!--img src="images/team-img2.jpg" class="img-responsive" alt="team img 2"-->
    							<div class="team-des">
    								<h3><? get_section( 'home_page_products_heading_2', true ) ?></h3>
    								<span class="heading"><? get_section( 'home_page_products_sub_heading_2', true ) ?></span>
    								<p><? get_section( 'home_page_products_description_2', true ) ?></p>
    							</div>
    					</div>
    				</div>
    				<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="1.3s">
    					<div class="team-wrapper">
    						<!--img src="images/team-img3.jpg" class="img-responsive" alt="team img 3"-->
    							<div class="team-des">
    								<h3><? get_section( 'home_page_products_heading_3', true ) ?></h3>
    								<span class="heading"><? get_section( 'home_page_products_sub_heading_3', true ) ?></span>
    								<p><? get_section( 'home_page_products_description_3', true ) ?></p>
    							</div>
    					</div>
    				</div>
    				<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="1.6s">
    					<div class="team-wrapper">
    						<!--img src="images/team-img4.jpg" class="img-responsive" alt="team img 4"-->
    							<div class="team-des">
    								<h3><? get_section( 'home_page_products_heading_4', true ) ?></h3>
    								<span class="heading"><? get_section( 'home_page_products_sub_heading_4', true ) ?></span>
    								<p><? get_section( 'home_page_products_description_4', true ) ?></p>
    							</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</section>
    	<!-- end team -->

    	<!-- start service -->
    	<!--section id="service">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-12">
    					<h2 class="wow bounceIn" data-wow-offset="50" data-wow-delay="0.3s">OUR <span>AWESOME</span> THINGS</h2>
    				</div>
    				<div class="col-md-4 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
    					<i class="fa fa-laptop"></i>
    					<h4>Web Design</h4>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget diam et laoreet phasellus ut nisi id leo molestie. Adipiscing vitae vel quam proin eget mauris eget. Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget diam et laoreet phasellus ut nisi id leo molestie.</p>
    				</div>
    				<div class="col-md-4 active wow fadeIn" data-wow-offset="50" data-wow-delay="0.9s">
    					<i class="fa fa-cloud"></i>
    					<h4>Cloud Computing</h4>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget diam et laoreet phasellus ut nisi id leo molestie. Adipiscing vitae vel quam proin eget mauris eget. Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget diam et laoreet phasellus ut nisi id leo molestie.</p>
    				</div>
    				<div class="col-md-4 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
    					<i class="fa fa-cog"></i>
    					<h4>UX Design</h4>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget diam et laoreet phasellus ut nisi id leo molestie. Adipiscing vitae vel quam proin eget mauris eget. Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget diam et laoreet phasellus ut nisi id leo molestie.</p>
    				</div>
    			</div>
    		</div>
    	</section-->
    	<!-- end servie -->

    	<!-- start portfolio -->
    	<!--section id="portfolio">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-12">
    					<h2 class="wow bounceIn" data-wow-offset="50" data-wow-delay="0.3s"><span>AWESOME</span> PORTFOLIO</h2>
    				</div>
    				<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
    					   <img src="images/portfolio-img1.jpg" class="img-responsive" alt="portfolio img 1">
                                <div class="portfolio-overlay">
                                    <h4>Project One</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget dia.</p>
                                    <a href="#" class="btn btn-default">DETAIL</a>
                                </div>
                        </div>
    				</div>
    				<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
                           <img src="images/portfolio-img2.jpg" class="img-responsive" alt="portfolio img 2">
                                <div class="portfolio-overlay">
                                    <h4>Project Two</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget dia.</p>
                                    <a href="#" class="btn btn-default">DETAIL</a>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
                           <img src="images/portfolio-img3.jpg" class="img-responsive" alt="portfolio img 3">
                                <div class="portfolio-overlay">
                                    <h4>Project Three</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget dia.</p>
                                    <a href="#" class="btn btn-default">DETAIL</a>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
                           <img src="images/portfolio-img4.jpg" class="img-responsive" alt="portfolio img 4">
                                <div class="portfolio-overlay">
                                    <h4>Project Four</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget dia.</p>
                                    <a href="#" class="btn btn-default">DETAIL</a>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
                           <img src="images/portfolio-img3.jpg" class="img-responsive" alt="portfolio img 3">
                                <div class="portfolio-overlay">
                                    <h4>Project Five</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget dia.</p>
                                    <a href="#" class="btn btn-default">DETAIL</a>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
                           <img src="images/portfolio-img4.jpg" class="img-responsive" alt="portfolio img 4">
                                <div class="portfolio-overlay">
                                    <h4>Project Six</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget dia.</p>
                                    <a href="#" class="btn btn-default">DETAIL</a>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
                           <img src="images/portfolio-img1.jpg" class="img-responsive" alt="portfolio img 1">
                                <div class="portfolio-overlay">
                                    <h4>Project Seven</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget dia.</p>
                                    <a href="#" class="btn btn-default">DETAIL</a>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
                           <img src="images/portfolio-img2.jpg" class="img-responsive" alt="portfolio img 2">
                                <div class="portfolio-overlay">
                                    <h4>Project Eight</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget dia.</p>
                                    <a href="#" class="btn btn-default">DETAIL</a>
                                </div>
                        </div>
                    </div>
    			</div>
    		</div>
    	</section-->
    	<!-- end portfolio -->

    	<!-- start contact -->
    	<section id="contact">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-12">
    					<h2 class="wow bounceIn" data-wow-offset="50" data-wow-delay="0.3s"><? get_section('home_page_contact_heading', true ) ?></h2>
    				</div>
    				<div class="col-md-6 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-offset="50" data-wow-delay="0.9s">
    					<form action="/service/contact-form" method="post">
    						<label>NAME</label>
    						<input name="fullname" type="text" class="form-control" id="fullname">

                            <label>EMAIL</label>
    						<input name="email" type="email" class="form-control" id="email">

                            <label>MESSAGE</label>
    						<textarea name="message" rows="4" class="form-control" id="message"></textarea>

                            <input type="submit" class="form-control">
    					</form>
    				</div>
    				<div class="col-md-6 col-sm-6 col-xs-12 wow fadeInRight" data-wow-offset="50" data-wow-delay="0.6s">
    					<address>
    						<p class="address-title"><? get_section( 'home_page_address_heading', true ) ?></p>
    						<span><? get_section( 'home_page_address_blurb', true ) ?></span>
    						<p><i class="fa fa-phone"></i> <? get_section( 'phone_number', true ) ?></p>
    						<p><i class="fa fa-envelope-o"></i> <? get_section( 'email_address', true ) ?></p>
    						<p><i class="fa fa-map-marker"></i> <? get_section( 'company_address', true ) ?></p>
    					</address>
    					<ul class="social-icon">
    						<li><h4>WE ARE SOCIAL</h4></li>
    						<? get_section( 'twitter_handle' ) ? '<li><a href="https://twitter.com/' . get_section( 'twitter_handle' ) . '" class="fa fa-twitter"><span class="label"></span></a></li>' : '' ?>
                            <? get_section( 'facebook_handle' ) ? '<li><a href="https://facebook.com/' . get_section( 'facebook_handle' ) . '" class="fa fa-facebook"><span class="label"></span></a></li>' : '' ?>
                            <? get_section( 'instagram_handle' ) ? '<li><a href="https://instagram.com/' . get_section( 'instagram_handle' ) . '" class="fa fa-instagram"><span class="label"></span></a></li>' : '' ?>
    					</ul>
    				</div>
    			</div>
    		</div>
    	</section>
    	<!-- end contact -->

        <!-- start copyright -->
        <footer id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p class="wow bounceIn" data-wow-offset="50" data-wow-delay="0.3s">
                       	<? get_section('copyright', true) ?> - <? get_section('company_name', true) ?></p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end copyright -->
        <? display_admin_link() ?>

        <!-- SCRIPTS -->
         <!-- Plugin JavaScript -->


        <script src="/js/vendor/node_modules/angular/angular.js"></script>
        <script src="/js/vendor/node_modules/angular-aria/angular-aria.js"></script>
        <script src="/js/vendor/node_modules/angular-animate/angular-animate.js"></script>
        <script src="/js/vendor/node_modules/angular-material/angular-material.js"></script>

        <script src="/js/vendor/moment.js"></script>
        <script src="/js/vendor/quill/quill.min.js"></script>
        <script src="/js/vendor/quill/ng-quill.js"></script>
        <script src="/js/vendor/angular-drag-and-drop-lists/angular-drag-and-drop-lists.js"></script>
        <script src="<? base_path( '/core/static/js/jquery.dropotron.min.js' ) ?>"></script>

        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.6/angular-messages.min.js"></script> -->

        <script type="text/javascript">
            var app = angular.module('ReachHome', ['ngMaterial', function($interpolateProvider) {
                $interpolateProvider.startSymbol('<%');
                $interpolateProvider.endSymbol('%>');
            }]);

            app.filter('titlecase', function() {
                return function(s) {
                    s = ( s === undefined || s === null ) ? '' : s;
                    return s.toString().toLowerCase().replace( /\b([a-z])/g, function(ch) {
                        return ch.toUpperCase();
                    });
                };
            });

            app.filter('moneyFormat', function() {
                return function(num) {
                  return '$' + parseInt(num).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                };
            });

            app.controller('HomePageController',  ['$scope', '$http',  function ($scope, $http) {
                $scope.errors = {};
                $scope.response = {};
                $scope.data = {};

                $scope.getInstitutes = function(data){
                    $http.get( '/public/http/services.form.getTertiaryInstitutes' ).then(
                    function ( results ) {
                        data = results.data;
                        $scope.institutes = data;
                    },
                    function () {} );
                };

                $scope.getInstitutes();


                $scope.getCourses = function(){
                  $http.get( '/public/http/services.form.getCourses?id='+$scope.institute ).then(
                    function ( results ) {
                        $scope.courses = results.data;
                    },
                    function () {} );
                };


                $scope.getCourseDetails = function(){
                  $http.get( '/public/http/services.form.getCourseDetails?id='+$scope.course ).then(
                    function ( results ) {
                        $scope.courseDetails = results.data;
                    },
                    function () {} );
                };

            }]);

            function currencyFormat(num) {
              return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            }

        </script>

	</body>
</html>
<!-- @layout() -->
