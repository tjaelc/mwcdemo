				<div id="footer-wrapper">
					<footer id="footer" class="container">
						<div class="row">
							<div class="3u 6u(medium) 12u$(small)">

								<!-- Links -->
									<section class="widget links">
										<h3><? get_section( 'footer_1_title' ) ?></h3>
										<ul class="style2">
											<? footer_links( get_section( 'footer_1_links' ) ) ?>
										</ul>
									</section>

							</div>
							<div class="3u 6u$(medium) 12u$(small)">

								<!-- Links -->
									<section class="widget links">
										<h3><? get_section( 'footer_2_title' ) ?></h3>
										<ul class="style2">
											<? footer_links( get_section( 'footer_2_links' ) ) ?>
										</ul>
									</section>

							</div>
							<div class="3u 6u(medium) 12u$(small)">

								<!-- Links -->
									<section class="widget links">
										<h3><? get_section( 'footer_3_title' ) ?></h3>
										<ul class="style2">
											<? footer_links( get_section( 'footer_3_links' ) ) ?>
										</ul>
									</section>

							</div>
							<div class="3u 6u$(medium) 12u$(small)">

								<!-- Contact -->
									<section class="widget contact">
										<h3>Contact Us</h3>
										<ul>
											<? get_section( 'twitter_handle' ) ? '<li><a href="https://twitter.com/' . get_section( 'twitter_handle' ) . '" class="icon fa-twitter"><span class="label">Twitter</span></a></li>' : '' ?>
											<? get_section( 'facebook_handle' ) ? '<li><a href="https://facebook.com/' . get_section( 'facebook_handle' ) . '" class="icon fa-facebook"><span class="label">Facebook</span></a></li>' : '' ?>
											<? get_section( 'instagram_handle' ) ? '<li><a href="https://instagram.com/' . get_section( 'instagram_handle' ) . '" class="icon fa-instagram"><span class="label">Instagram</span></a></li>' : '' ?>
											<? get_section( 'dribbble_handle' ) ? '<li><a href="https://dribbble.com/' . get_section( 'dribbble_handle' ) . '" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>' : '' ?>
											<? get_section( 'pinterest_handle' ) ? '<li><a href="https://pinterest.com/' . get_section( 'pinterest_handle' ) . '" class="icon fa-pinterest"><span class="label">Pinterest</span></a></li>' : '' ?>
										</ul>
										<p><? nl2br( get_section( 'company_address' ) ) ?></p>
									</section>

							</div>
						</div>
						<div class="row">
							<div class="12u">
								<div id="copyright">
									<ul class="menu">
										<li><? get_section('copyright') ?></li><li><? get_section('company_name') ?></li>
									</ul>
								</div>
							</div>
						</div>
					</footer>
				</div>