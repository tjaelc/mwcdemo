<script>window.jQuery || document.write('<script src="/js/vendor/jquery-2.2.1.min.js"><\/script>')</script>

<script src="<? base_path( '/core/static/js/jquery.js' ) ?>"></script>
		<script src="<? base_path( '/core/static/js/bootstrap.min.js' ) ?>"></script>
        <script src="<? base_path( '/core/static/js/jquery.singlePageNav.min.js' ) ?>"></script>
		<script src="<? base_path( '/core/static/js/typed.js' ) ?>"></script>
		<script src="<? base_path( '/core/static/js/wow.min.js' ) ?>"></script>
		<script src="<? base_path( '/core/static/js/custom.js' ) ?>"></script>

	    <script src="/js/vendor/node_modules/angular/angular.js"></script> 
	    <script src="/js/vendor/node_modules/angular-aria/angular-aria.js"></script> 
	    <script src="/js/vendor/node_modules/angular-animate/angular-animate.js"></script> 
	    <script src="/js/vendor/node_modules/angular-material/angular-material.js"></script>
	    <script src="/js/vendor/svg-assets-cache.js"></script>
	    <script src="/js/vendor/tinycolor-min.js"></script>
	    <script src="/js/vendor/moment.js"></script>
	    <script src="/js/vendor/md-color-picker/mdColorPicker.js"></script>
	    <script src="/js/vendor/ace-builds-master/src-noconflict/ace.js"></script>
	    <script src="/js/vendor/ui-ace-master/src/ui-ace.js"></script>
	    <script src="/js/vendor/quill/quill.min.js" type="text/javascript"></script>
	    <script src="/js/vendor/quill/ng-quill.js" type="text/javascript"></script>
	    <script src="/js/vendor/angular-drag-and-drop-lists/angular-drag-and-drop-lists.js"></script>

	    {{-- <script src="<? base_path( '/core/static/js/jquery.min.js' ) ?>"></script> --}}
		<script src="<? base_path( '/core/static/js/jquery.dropotron.min.js' ) ?>"></script>

		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  {{-- <link rel="stylesheet" href="/resources/demos/style.css"> --}}
  {{--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>--}}
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>

  jQuery('body').on('focus',".datepicker-past", function(){
	    jQuery(this).datepicker({
	    	changeMonth: true,
	      changeYear: true,
	      yearRange: "-100:+0"
	    });
	});

	jQuery('body').on('focus',".datepicker-future", function(){
	    jQuery(this).datepicker({
	    	changeMonth: true,
	      changeYear: true,
	      yearRange: "-0:+50"
	    });
	});

	jQuery('body').on('focus',".datepicker", function(){
	    jQuery(this).datepicker({
	    	changeMonth: true,
	      changeYear: true,
	      yearRange: "-100:+50"
	    });
	});
	
  </script>


		<script src="<? base_path( '/core/static/js/skel.min.js' ) ?>"></script>
		<script src="<? base_path( '/core/static/js/util.js' ) ?>"></script>
		<!--[if lte IE 8]><script src="<? base_path( '/core/static/js/ie/respond.min.js' ) ?>"></script><![endif]-->
		<script src="<? base_path( '/core/static/js/main.js' ) ?>"></script>
		<script src="<? base_path( '/core/static/js/controllers/FormDataController.js' ) ?>"></script>