header
{
	background: <? return get_colour( 'top-bar-colour' ) ?>;
}

header a, header span, header i, header p
{
	color: <? return get_colour( 'top-bar-text-colour' ) ?>;
}

.homepage-body
{
		background:  <? return get_colour( 'homepage-background-colour' ) ?>;
		color:  <? return get_colour( 'homepage-text-colour' ) ?>;

}

#home
{
	background-color:  <? return get_colour( 'homepage-lead-image-background-colour' ) ?>;

}

#home a.btn-default {
	background: <? return get_colour( 'homepage-lead-link-background-colour' ) ?> !important;
	color: <? return get_colour( 'homepage-lead-link-text-colour' ) ?> !important;
	border: solid 1px <? return get_colour( 'homepage-lead-link-border-colour' ) ?> !important;
}

.content-body {

  color:  <? return get_colour( 'page-content-text-colour' ) ?>;
  background:  <? return get_colour( 'page-content-background-colour' ) ?>;
  
}

#products
{

	background-color: <? get_colour( 'homepage-contrast-background-colour' )?>;
}

#products .team-des {
	background: <? get_colour( 'homepage-products-background-colour' )?>;
}

#products .team-wrapper {
	box-shadow: none !important;
	box-sizing: none !important;
	-webkit-tap-highlight-color: none !important;
}

#copyright
{
	background-color: <? get_colour( 'footer-background-colour' )?>;
	color: <? get_colour( 'footer-text-colour' )?>;

}